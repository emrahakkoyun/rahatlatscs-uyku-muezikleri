package sanalmicro.com.rahatlatciuykumuzikleri;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Start extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Button baslat = (Button)findViewById(R.id.baslat);
       final  Intent  in = new Intent(Start.this,MainActivity.class);
        baslat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             startActivity(in);
            }
        });

    }
}
