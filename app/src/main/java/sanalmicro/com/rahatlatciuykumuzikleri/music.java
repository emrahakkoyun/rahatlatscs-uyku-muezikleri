package sanalmicro.com.rahatlatciuykumuzikleri;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class music extends Activity {
    Boolean ts=true;
    Integer[] muzik = {
            R.raw.a1,
            R.raw.a2,
            R.raw.a3,
            R.raw.a4,
            R.raw.a5,
            R.raw.a6,
            R.raw.a7,
            R.raw.a8,
            R.raw.a9,
            R.raw.a10
    };
    Integer[] resim = {
            R.drawable.so1,
            R.drawable.so2,
            R.drawable.so3,
            R.drawable.so4,
            R.drawable.so5,
            R.drawable.so6,
            R.drawable.so7,
            R.drawable.so8,
            R.drawable.so9,
            R.drawable.so10

    };
    MediaPlayer media;
    MediaPlayer media1;
    MediaPlayer media2;
    MediaPlayer media3;
    MediaPlayer media4;
    MediaPlayer media5;
    MediaPlayer media6;
    int getid;
    private SeekBar volumeSeekbar = null;
    private AudioManager audioManager = null;
    LinearLayout linearLayout;
    ImageView ib1,ib2,pi1,pi2,pi3,pi4,pi5,pi6;
    Boolean pi1control=true;
    Boolean pi2control=true;
    Boolean pi3control=true;
    Boolean pi4control=true;
    Boolean pi5control=true;
    Boolean pi6control=true;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        MobileAds.initialize(this,
                "ca-app-pub-1312048647642571~7473193049");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
         ib1= (ImageView) findViewById(R.id.blt1);
         ib2= (ImageView) findViewById(R.id.blt2);
         pi1= (ImageView) findViewById(R.id.pi1);
         pi2= (ImageView) findViewById(R.id.pi2);
         pi3= (ImageView) findViewById(R.id.pi3);
         pi4= (ImageView) findViewById(R.id.pi4);
         pi5= (ImageView) findViewById(R.id.pi5);
         pi6= (ImageView) findViewById(R.id.pi6);
        ib1.setImageResource(R.drawable.playb);
        linearLayout = (LinearLayout)findViewById(R.id.linearLayout);
        initControls();
        getid= getIntent().getExtras().getInt("id",0);
        linearLayout.setBackgroundResource(resim[getid]);
      //  Toast.makeText(getApplicationContext(),String.valueOf(data),Toast.LENGTH_LONG).show();
         play();
        ib1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             play();
            }
        });
        ib2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
stop();
            }
        });
        pi1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sounds(1);
            }
        });
        pi2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sounds(2);
            }
        });
        pi3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sounds(3);
            }
        });
        pi4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sounds(4);
            }
        });
        pi5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sounds(5);
            }
        });
        pi6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sounds(6);
            }
        });
    }
    private void initControls()
    {
        try
        {
            volumeSeekbar = (SeekBar)findViewById(R.id.seekBar);
            volumeSeekbar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
            volumeSeekbar.getThumb().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            volumeSeekbar.setMax(audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            volumeSeekbar.setProgress(audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC));


            volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onStopTrackingTouch(SeekBar arg0)
                {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0)
                {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2)
                {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void sounds(int gel){
        switch (gel)
        {
            case 1:{
                if(pi1control==true){
                    play1();
                    pi1control=false;
                }
                else {
                    stop1();
                    pi1control=true;
                }
            break;}
            case 2:{
                if(pi2control==true){
                    play2();
                    pi2control=false;
                }
                else {
                    stop2();
                    pi2control=true;
                }
                break;}
            case 3:{
                if(pi3control==true){
                    play3();
                    pi3control=false;
                }
                else {
                    stop3();
                    pi3control=true;
                }
                break;}
            case 4:{
                if(pi4control==true){
                    play4();
                    pi4control=false;
                }
                else {
                    stop4();
                    pi4control=true;
                }
                break;}
            case 5:{
                if(pi5control==true){
                    play5();
                    pi5control=false;
                }
                else {
                    stop5();
                    pi5control=true;
                }
                break;}
            case 6:{
                if(pi6control==true){
                    play6();
                    pi6control=false;
                }
                else {
                    stop6();
                    pi6control=true;
                }
                break;}

        }
    }
    public void play(){
        ib1.setImageResource(R.drawable.playb);
        ib2.setImageResource(R.drawable.pause);
        if(media==null) {
            media= MediaPlayer.create(this,muzik[getid]);
            media.setVolume(1.0f, 1.0f);
            media.start();
            media.setLooping(true);}
    }
    public void stop(){
        ib1.setImageResource(R.drawable.play);
        ib2.setImageResource(R.drawable.pauseb);
        media.release();
        media=null;
    }
    public void play1(){
        pi1.setImageResource(R.drawable.pi1b);
        if(media1==null) {
            media1= MediaPlayer.create(this,R.raw.piano);
            media1.setVolume(1.0f, 1.0f);
            media1.start();
            media1.setLooping(true);}
    }
    public void stop1(){
        pi1.setImageResource(R.drawable.pi1);
        media1.release();
        media1=null;
    }
    public void play2(){
        pi2.setImageResource(R.drawable.pi2b);
        if(media2==null) {
            media2= MediaPlayer.create(this,R.raw.bird);
            media2.setVolume(1.0f, 1.0f);
            media2.start();
            media2.setLooping(true);}
    }
    public void stop2(){
        pi2.setImageResource(R.drawable.pi2);
        media2.release();
        media2=null;
    }
    public void play3(){
        pi3.setImageResource(R.drawable.pi3b);
        if(media3==null) {
            media3= MediaPlayer.create(this,R.raw.wolf);
            media3.setVolume(1.0f, 1.0f);
            media3.start();
            media3.setLooping(true);}
    }
    public void stop3(){
        pi3.setImageResource(R.drawable.pi3);
        media3.release();
        media3=null;
    }
    public void play4(){
        pi4.setImageResource(R.drawable.pi4b);
        if(media4==null) {
            media4= MediaPlayer.create(this,R.raw.ney);
            media4.setVolume(1.0f, 1.0f);
            media4.start();
            media4.setLooping(true);}
    }
    public void stop4(){
        pi4.setImageResource(R.drawable.pi4);
        media4.release();
        media4=null;
    }
    public void play5(){
        pi5.setImageResource(R.drawable.pi5b);
        if(media5==null) {
            media5= MediaPlayer.create(this,R.raw.wind);
            media5.setVolume(1.0f, 1.0f);
            media5.start();
            media5.setLooping(true);}
    }
    public void stop5(){
        pi5.setImageResource(R.drawable.pi5);
        media5.release();
        media5=null;
    }
    public void play6(){
        pi6.setImageResource(R.drawable.pi6b);
        if(media6==null) {
            media6= MediaPlayer.create(this,R.raw.rain);
            media6.setVolume(1.0f, 1.0f);
            media6.start();
            media6.setLooping(true);}
    }
    public void stop6(){
        pi6.setImageResource(R.drawable.pi6);
        media6.release();
        media6=null;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(media != null && media.isPlaying()) {
            media.stop();
        }
        if(media1 != null && media1.isPlaying()) {
            media1.stop();
        }
        if(media2 != null && media2.isPlaying()) {
            media2.stop();
        }
        if(media3 != null && media3.isPlaying()) {
            media3.stop();
        }
        if(media4 != null && media4.isPlaying()) {
            media4.stop();
        }
        if(media5 != null && media5.isPlaying()) {
            media5.stop();
        }
        if(media6 != null && media6.isPlaying()) {
            media6.stop();
        }
    }
}
