package sanalmicro.com.rahatlatciuykumuzikleri;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class MainActivity extends Activity {
    private InterstitialAd mInterstitial;
    Intent intent;
    public Integer[] resimler = {
            R.drawable.resim1,
            R.drawable.resim2,
            R.drawable.resim3,
            R.drawable.resim4,
            R.drawable.resim5,
            R.drawable.resim6,
            R.drawable.resim7,
            R.drawable.resim8,
            R.drawable.resim9,
            R.drawable.resim10

    };
    public String[] yazilar = {
            "Kar Yağarken",
            "Yağmur Yağarken",
            "Ormanlık Alandasın",
            "Şelale Başındasın",
            "Gün Batımını izleken ",
            "Yılbaşı Günündesin",
            "Çocuklarla oynarken",
            "Okyanusun ortasındasın",
            "Dağ Yamaçlarındasın",
            "Sahildesin"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ImageAdapter adapter = new ImageAdapter(MainActivity.this, yazilar, resimler);
        GridView grid=(GridView)findViewById(R.id.grid);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                inrequestadd(position);

            }
        });
    }
    public void inrequestadd(final int position) {
        intent = new Intent(MainActivity.this,music.class);
        intent.putExtra("id", position);
        startActivity(intent);
    }
}
